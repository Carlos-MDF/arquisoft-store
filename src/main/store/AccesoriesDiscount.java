package store;

public class AccesoriesDiscount implements DiscountCalculator {
	
	@Override
	public float calculateDiscount(OrderItem item) {
		float accesoriesDiscount = 0;
		if (item.calculateTotalAmount() >= 100) {
			accesoriesDiscount = item.calculateTotalAmount() * 10 / 100;
		}
		return accesoriesDiscount;
	}
}
