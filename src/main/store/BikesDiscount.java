package store;

public class BikesDiscount implements DiscountCalculator {

	@Override
	public float calculateDiscount(OrderItem item) {
		float bikeDiscount = 0;
		bikeDiscount = item.calculateTotalAmount() * 20 / 100;
		return bikeDiscount;
	}
}
